package ictgradschool.industry.lab_swingii.bounce;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageShape extends Shape {
   BufferedImage dino = null;

    public ImageShape() {


        try
        {
            dino = ImageIO.read(new File("dino.png"));
        }
        catch(
                IOException e)
        {}
        fWidth = dino.getWidth();
        fHeight = dino.getHeight();
    }

    public ImageShape(int x, int y) {
        super(x, y);
    }

    public ImageShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }




    @Override
    public void paint(Painter painter) {
        painter.drawImage(dino, fX, fY);


    }
}
